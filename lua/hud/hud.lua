if not WatchOutForEnemies then
	dofile(ModPath .. "Core.lua")
end

local HUDSettings = WOE_Settings_Hud

if not HUDSettings.Hud_Switch then
	return
end

local alpha = 0
local isopen = true

local camera; local from; local to; local raycast;
function HUDManager:woe_update(t, dt)
 	local ws = managers.hud._workspace
	local cam = managers.viewport:get_current_camera()
	local player = managers.player:local_player()
	
	if not Utils:IsInHeist() or not cam or not alive(player) then
		return
	end
	
	local HUDSettings = WatchOutForEnemies.settings.Hud
	
	if managers.player:player_unit() then
		camera = managers.player:player_unit():camera()
		from = camera:position()
		to = camera:position() + camera:rotation():y() * 50000
		raycast = World:raycast("ray", from, to, "slot_mask", managers.slot:get_mask("enemies", "bullet_blank_impact_targets"))
	end

	self._enemy_unit = raycast and (managers.enemy:is_enemy(raycast.unit) and raycast.unit or self._enemy_unit) or self._enemy_unit

	if alive(self._enemy_unit) and self._enemy_unit:character_damage():dead() then
		self._enemy_unit = nil
		alpha = 0
	end
	
	if alive(self._enemy_unit) then
		local unit_name = HopLib:name_provider():name_by_unit(self._enemy_unit) or HopLib:name_provider():name_by_id(self._enemy_unit:base()._tweak_table) or WOE_Localization('ene_'..tostring(self._enemy_unit:base()._tweak_table)) or '敌军'

		local weapon_unit = self._enemy_unit:inventory():equipped_unit()
		local weapon_name = ''
		if weapon_unit then
			weapon_name = string.gsub(tostring(weapon_unit:base()._name_id),'_npc','',1)
		else
			weapon_name = WOE_Localization("WOE_Hud_on_dominated")
		end

		local unit_health = (self._enemy_unit:character_damage()._health)*10 or WOE_Localization("WOE_Health_get_failed")
		local max_unit_health = (self._enemy_unit:character_damage()._HEALTH_INIT)*10 or WOE_Localization("WOE_Health_get_failed")

		local unit_distance = mvector3.direction(Vector3(), self._enemy_unit:movement():m_com(), managers.player:player_unit():position())/100
		local unit_distance = unit_distance - unit_distance%1

		local hazard_level = WOE_getHazardLevel(unit_health, unit_distance)

		local can_surrender = check_surrender(self._enemy_unit) and WOE_Localization("WOE_Able") or WOE_Localization("WOE_Unable")    --这句为判断

		local dis = mvector3.distance(player:position(), self._enemy_unit:position()) > HUDSettings.Hud_Size and HUDSettings.Hud_Size or mvector3.distance(player:position(), self._enemy_unit:position())

		local screen_pos = ws:world_to_screen(cam, self._enemy_unit:position())
		local x = math.clamp(screen_pos.x, 128, self._spedometer_hud:parent():w()) + 150 - (dis  / 25)
		local y = math.clamp(screen_pos.y, 128, self._spedometer_hud:parent():w()) - 100  + (dis  / 40)

		local health_rect = unit_health / max_unit_health
		local hazard_rect = hazard_level/10
		local unit_health = unit_health - unit_health%1

		if raycast and raycast.unit and managers.enemy:is_enemy(raycast.unit) then
				alpha = 2
		end
		
		self._spedometer_box:set_text(
		"\n\n "..WOE_Localization("WOE_Hud_unit_name").." : "..unit_name
		..'\n\n '..WOE_Localization("WOE_Hud_weapon_name") .. " : "..weapon_name
		..'\n\n '..WOE_Localization("WOE_Hud_unit_distance").." : "..unit_distance
		..'\n\n '..WOE_Localization("WOE_Hud_unit_health").." : "..unit_health
		..'\n\n '..WOE_Localization("WOE_Hud_hazard").." : "..hazard_level
		..'\n\n '..WOE_Localization("WOE_Can_surrender").." : "..can_surrender
		)
		
		self._spedometer_hud:set_visible(true)
		self._spedometer_hud:set_size(self._spedometer_init.w - dis  / 60, self._spedometer_init.h - dis / 30)
		self._spedometer_bg_box:set_size(self._spedometer_hud:w(), self._spedometer_hud:h())
		self._spedometer_box:set_font_size(self._spedometer_hud:w() / 7)
		
		bote_unit_health:set_w(self._spedometer_hud:w() * 2)
		self._bote_health_background:set_w(self._spedometer_hud:w() * 2 + 1)
		bote_hazard_level:set_w(self._spedometer_hud:w() * 2)
		self._bote_hazard_background:set_w(self._spedometer_hud:w() * 2 + 1)

		self._spedometer_hud:set_center(x+dis/60, y+dis/300)

		bote_unit_health:set_center(self._spedometer_bg_box:left() - 6.5, self._spedometer_bg_box:right() - 0.13)
		bote_hazard_level:set_center(self._spedometer_bg_box:left() - 19, self._spedometer_bg_box:right() - 0.13)

		self._bote_health_background:set_center(self._spedometer_bg_box:left() - 6.5, self._spedometer_bg_box:right())
		self._bote_hazard_background:set_center(self._spedometer_bg_box:left() - 19, self._spedometer_bg_box:right())

		bote_unit_health:set_texture_rect(self._bote_health_rect[1], self._bote_health_rect[2], self._bote_health_rect[3] / health_rect, self._bote_health_rect[4])
		bote_hazard_level:set_texture_rect(self._bote_hazard_rect[1], self._bote_hazard_rect[2], self._bote_hazard_rect[3] / hazard_rect, self._bote_hazard_rect[4])
		
		self:bote_visible(HUDSettings.Hud_Fading, true, alpha)

		if x > 1200 or x < 300 then
			self._enemy_unit = nil
		end
	end
	
    if raycast and managers.enemy:is_enemy(raycast.unit) then
		--Noting
	else
		alpha = alpha >=0 and alpha - dt or 0
		self:bote_visible(HUDSettings.Hud_Fading, false, alpha)
		if alpha == 0 then
			self._enemy_unit = nil
		end
	end
	
	if not self._enemy_unit then
		if alpha == 2 then
			alpha = 1
		end
		
		alpha = alpha >=0 and alpha - dt * 30 or 0
	end
end

Hooks:PostHook(HUDManager, "_setup_player_info_hud_pd2","init_hud", function(self)
	local hud_script = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2)
	local in_panel = hud_script and hud_script.panel
	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2)
	local hud_panel = hud.panel
	if alive(in_panel) then
		self._bote_w_h = {w = 150,h = 100}
		self._spedometer_init = {w = 100,h = 200}
		self._spedometer_hud = hud_panel:panel({
			name = "bota_hud",
			w = 100,
			h = 200
		})

		self._spedometer_box = self._spedometer_hud:text({
			color = Color(1,1,1,1),
			font = tweak_data.hud_players.ammo_font,
			text = '',
			alpha = 1,
			vertical = "top",
			align = "left",
			font_size = 15
		})

		self._spedometer_bg_box = HUDBGBox_create(self._spedometer_hud, {
				alpha = 1
			},
			{
				blend_mode = "normal",
				color = Color(1, 1, 1, 1)
			}
		)

		self._boteHUD_unit_health = self._spedometer_hud:panel({
			name 	= "Bote_unit_health",
			halign 	= "grow",
			valign 	= "grow"
		})

		self._boteHUD_unit_health_panel = self._boteHUD_unit_health:panel({
			name 	= "bote_health_panel",
			visible = false
		})

		bote_unit_health = self._boteHUD_unit_health:bitmap({
			name = "bote_unit_health",
			texture = "guis/textures/pd2/healthshield",
			texture_rect = {2, 18, 232, 11},
			blend_mode 	= "normal"
		})

		bote_hazard_level = self._boteHUD_unit_health:bitmap({
			name = "bote_hazard_level",
			texture = "guis/textures/pd2/healthshield",
			texture_rect = {2, 18, 232, 11},
			blend_mode 	= "normal"
		})

		self._bote_health_background = self._boteHUD_unit_health:bitmap({
			name = "bote_health_background",
			texture = "guis/textures/pd2/healthshield",
			texture_rect = {1, 1, 234, 13},
			blend_mode = "normal"
		})
		self._bote_hazard_background = self._boteHUD_unit_health:bitmap({
			name = "bote_hazard_background",
			texture = "guis/textures/pd2/healthshield",
			texture_rect = {1, 1, 234, 13},
			blend_mode 	= "normal"

		})

		self._bote_health_rect = {2, 18, 232, 11}
		self._bote_hazard_rect = {2, 18, 232, 11}

		bote_unit_health:set_w(self._bote_w_h.w + 50)
		self._bote_health_background:set_w(self._bote_w_h.w + 51)
		bote_unit_health:set_rotation(-90)
		self._bote_health_background:set_rotation(-90)

		bote_hazard_level:set_w(self._bote_w_h.w + 50)
		self._bote_hazard_background:set_w(self._bote_w_h.w + 51)
		bote_hazard_level:set_rotation(-90)
		self._bote_hazard_background:set_rotation(-90)
		self:bote_set_color()
	end
end)

Hooks:PostHook(HUDManager, "update","bote_update_hud", function(self, t, dt)
    if self._spedometer_hud then
		self:woe_update(t, dt)
    end
end)

function check_surrender(unit)   -- 检查是否可以变节
	if not alive(unit) then
		return false
	end

	local unit_surrender = tweak_data.character[unit:base()._tweak_table].surrender

	if not unit_surrender or unit_surrender == tweak_data.character.presets.surrender.special then
		return false
	else
		return true
	end
end

function HUDManager:bote_set_color()
	local HudColorA = WatchOutForEnemies.settings.Hud.Hud_Color.Hud_Color_a
	local HudColorB = WatchOutForEnemies.settings.Hud.Hud_Color.Hud_Color_b
	bote_unit_health:set_color(Color(HudColorA.Hud_a_R/255, HudColorA.Hud_a_G/255, HudColorA.Hud_a_B/255))
	bote_hazard_level:set_color(Color(HudColorB.Hud_b_R/255, HudColorB.Hud_b_G/255, HudColorB.Hud_b_B/255))
end

function HUDManager:bote_visible(is_gradient, mode, vis)
	local data = (is_gradient and (mode and vis or vis) or (mode and 1 or 0))

	self._spedometer_box:set_alpha(data)
	self._spedometer_bg_box:set_alpha(is_gradient and (vis > 1 and 2 or (vis * 2)) or data)
	self._boteHUD_unit_health:set_alpha(data)
	self._boteHUD_unit_health_panel:set_alpha(data)
end