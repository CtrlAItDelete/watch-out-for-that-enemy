if not WatchOutForEnemies then
	dofile(ModPath .. "Core.lua")
end

if not WatchOutForEnemies.settings.EnemySpotter.ES_Enable then
	return
end

local WOE = WatchOutForEnemies.settings.EnemySpotter
local ES_Filter = WatchOutForEnemies.settings.EnemySpotter.ES_Filter
if RequiredScript == "lib/managers/enemymanager" then
	EnemySpotter = EnemySpotter or class()
	
	local Idstring_unit = {
		america = {
			dozer_1 = Idstring("units/payday2/characters/ene_bulldozer_1/ene_bulldozer_1"),
			dozer_2 = Idstring("units/payday2/characters/ene_bulldozer_2/ene_bulldozer_2"),
			dozer_3 = Idstring("units/payday2/characters/ene_bulldozer_3/ene_bulldozer_3"),
			zeal_dozer_1 = Idstring("units/pd2_dlc_gitgud/characters/ene_zeal_bulldozer/ene_zeal_bulldozer"),
			zeal_dozer_2 = Idstring("units/pd2_dlc_gitgud/characters/ene_zeal_bulldozer_2/ene_zeal_bulldozer_2"),
			zeal_dozer_3 = Idstring("units/pd2_dlc_gitgud/characters/ene_zeal_bulldozer_3/ene_zeal_bulldozer_3")
		},
		russia = {
			dozer_1 = Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_tank_r870/ene_akan_fbi_tank_r870"),
			dozer_2 = Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_tank_saiga/ene_akan_fbi_tank_saiga"),
			dozer_3 = Idstring("units/pd2_dlc_mad/characters/ene_akan_fbi_tank_rpk_lmg/ene_akan_fbi_tank_rpk_lmg")
		},
		zombie = {
			dozer_1 = Idstring("units/pd2_dlc_hvh/characters/ene_bulldozer_hvh_1/ene_bulldozer_hvh_1"),
			dozer_2 = Idstring("units/pd2_dlc_hvh/characters/ene_bulldozer_hvh_2/ene_bulldozer_hvh_2"),
			dozer_3 = Idstring("units/pd2_dlc_hvh/characters/ene_bulldozer_hvh_3/ene_bulldozer_hvh_3")
		},
		murkywater = {
			dozer_1 = Idstring("units/pd2_dlc_bph/characters/ene_murkywater_bulldozer_1/ene_murkywater_bulldozer_1"),
			dozer_2 = Idstring("units/pd2_dlc_bph/characters/ene_murkywater_bulldozer_2/ene_murkywater_bulldozer_2"),
			dozer_3 = Idstring("units/pd2_dlc_bph/characters/ene_murkywater_bulldozer_3/ene_murkywater_bulldozer_3")
		},
		federales = {
			dozer_1 = Idstring("units/pd2_dlc_bex/characters/ene_swat_dozer_policia_federale_r870/ene_swat_dozer_policia_federale_r870"),
			dozer_2 = Idstring("units/pd2_dlc_bex/characters/ene_swat_dozer_policia_federale_saiga/ene_swat_dozer_policia_federale_saiga"),
			dozer_3 = Idstring("units/pd2_dlc_bex/characters/ene_swat_dozer_policia_federale_m249/ene_swat_dozer_policia_federale_m249")
		}
	}
	
	local icons_atlas = "guis/textures/pd2/skilltree/icons_atlas"
	local modifiers_atlas = "guis/dlcs/cee/textures/pd2/crime_spree/modifiers_atlas"
	local modifiers_atlas_2 = "guis/dlcs/drm/textures/pd2/crime_spree/modifiers_atlas_2"
	local preplan_icon_types = "guis/dlcs/big_bank/textures/pd2/pre_planning/preplan_icon_types"
	local achievement_atlas_cac = "guis/dlcs/trk/atlases/achievement_atlas_cac"
	local achievements_atlas = "guis/dlcs/trk/textures/pd2/achievements_atlas"
	local achievements_atlas3 = "guis/dlcs/trk/textures/pd2/achievements_atlas3"
	local achievements_atlas6 = "guis/dlcs/trk/textures/pd2/achievements_atlas6"
	local achievements_atlas7 = "guis/dlcs/trk/textures/pd2/achievements_atlas7"
	
	local icon = {
		medic = {
			path = icons_atlas, 
			texture_rect = {
				64 * 5, 
				64 * 7, 
				64, 
				64
			}
		},
		taser = {
			path = preplan_icon_types, 
			texture_rect = {
				48 * 3, 
				48 * 3, 
				48, 
				48
			}
		},
		spooc = {
			path = achievement_atlas_cac, 
			texture_rect = {
				611, 
				350, 
				85, 
				85
			}
		},
		shield = {
			path = modifiers_atlas, 
			texture_rect = {
				128 * 3, 
				128 * 0, 
				128, 
				128
			}
		},
		tank = {
			path = icons_atlas, 
			texture_rect = {
				64 * 3, 
				64, 
				64, 
				64
			}
		},
		tank_img = {
			path = modifiers_atlas, 
			texture_rect = {
				128 * 2,
				128 * 2,
				128, 
				128
			},
		},
		tank_medic = {
			path = modifiers_atlas_2, 
			texture_rect = {
				128 * 0, 
				128 * 1, 
				128, 
				128
			}
		},
		tank_mini = {
			path = modifiers_atlas_2, 
			texture_rect = {
				128 * 3, 
				128 * 1, 
				128, 
				128
			}
		},
		sniper = {
			path = modifiers_atlas_2,
			texture_rect = {
				128 * 3, 
				128 * 2, 
				128, 
				128
			}
		},
		heavy_swat_sniper = {
			path = modifiers_atlas_2, 
			texture_rect = {
				128 * 3, 
				128 * 2, 
				128, 
				128
			}
		},
		phalanx_minion = {
			path = modifiers_atlas_2, 
			texture_rect = {
				128 * 0, 
				128 * 0, 
				128, 
				128
			}
		},
		unknown_gas_for_fire_mask = {
			path = achievements_atlas6, 
			texture_rect = {
				174, 
				174, 
				85, 
				85
			}
		},
		summers = {
			path = achievements_atlas7,
			texture_rect = {
				0,
				696,
				85,
				85
			}
		},
		snowman_boss = {
			path = achievements_atlas6,
			texture_rect = {
				530,
				-25,
				85,
				85
			},
		},
		unknown_winter_2 = {
			path = achievements_atlas,
			texture_rect = {
				609,
				696,
				85,
				85
			}
		},
		medic_summers = {
			path = achievements_atlas3,
			texture_rect = {
				348,
				522,
				85,
				85
			}
		},
		hrt_titan = {
			path = achievements_atlas7,
			texture_rect = {
				522,
				609,
				85,
				85
			}
		},
		taser_summers = {
			path = achievements_atlas6,
			texture_rect = {
				348,
				174,
				85,
				85
			}
		},
		taser_titan = {
			path = achievements_atlas6,
			texture_rect = {
				348,
				174,
				85,
				85
			}
		},
		tank_titan = {
			path = achievements_atlas7,
			texture_rect = {
				174,
				261,
				85,
				85
			}
		},
		tank_titan_assault = {
			path = achievements_atlas7,
			texture_rect = {
				174,
				261,
				85,
				85
			}
		},
		phalanx_minion_assault = {
			path = modifiers_atlas_2, 
			texture_rect = {
				128 * 0, 
				128 * 0, 
				128, 
				128
			}
		},
	}
	
	function EnemySpotter:new()
		local tb = {}
		setmetatable(tb, self)
		self.__index = self
		return tb
	end
	
	function EnemySpotter:init(unit)
		self._unit = unit
		self._tweak_table = self._unit:base()._tweak_table
		self._name = self._unit:name()
		self._update_time = true
		self._raycast_time = true
		
		self:add_spotter_hud()
		self:add_update()
	end

	function EnemySpotter:add_spotter_hud()
		if self._tweak_table == "tank" then
			for key, name in pairs(Idstring_unit) do
				if self._name == name.dozer_1 or self._name == name.zeal_dozer_2 then
					self._tank_color = Color(140 / 255, 220 / 255, 0)
					break
				end
				
				if self._name == name.dozer_2 or self._name == name.zeal_dozer_3  then
					self._tank_color = Color(70 / 255, 70 / 255, 70 / 255)
					break
				end
				
				if self._name == name.dozer_3 or self._name == name.zeal_dozer_1  then
					self._tank_lmg_path = icon.tank_img.path
					self._tank_lmg_texture_rect = icon.tank_img.texture_rect
					break
				end
			end
		end
		
		local texture = self._tank_lmg_path or icon[self._tweak_table].path
		local texture_rect = self._tank_lmg_texture_rect or icon[self._tweak_table].texture_rect
		local hud_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2).panel
		local color = self._tank_color or Color.white
		self._spotter_icon = hud_panel:bitmap({
			visible = false,
			name = "enemy_spotter_icon",
			texture = texture,
			texture_rect = texture_rect,
			alpha = -0.01,
			color = color,
			w = WOE.ES_IconSize,
			h = WOE.ES_IconSize
		})

		if self._tweak_table == "snowman_boss" then
			self._spotter_icon:set_w(WOE.ES_IconSize+5)
			self._spotter_icon:set_h(WOE.ES_IconSize+5)
		end
	end
	
	local SizeExt
	function EnemySpotter:set_size()
		if managers.enemy then
			for _, enemy in pairs(managers.enemy:all_enemies()) do
				if enemy and alive(enemy.unit) and enemy.unit:base() and enemy.unit:base().spotter then
					SizeExt = 0
					SizeExt = enemy.unit:base()._tweak_table == "snowman_boss" and 5 or 0
					enemy.unit:base().spotter._spotter_icon:set_w(WOE.ES_IconSize+SizeExt)
					enemy.unit:base().spotter._spotter_icon:set_h(WOE.ES_IconSize+SizeExt)
				end
			end
		end
	end
	
	function EnemySpotter:add_update()
		self._uptate_id = "update("..self._unit:id()..")"
		managers.hud:add_updator(self._uptate_id , callback(self, self, "update"))
	end
	
	function EnemySpotter:update(t, dt)
		if alive(self._unit) and not self._unit:character_damage():dead() then
			if self._update_time then	
				if not managers.player:player_unit() then
					if self._spotter_icon then
						self._spotter_icon:set_visible(false)
					end
					
					return
				end
				
				local current_camera = managers.viewport:get_current_camera()
				self._head_pos = self._unit:get_object(Idstring("Spine1")):position()
				self._screen_pos = managers.hud._workspace:world_to_screen(current_camera, self._head_pos)
				
				if self._spotter_icon:alpha() ~= -0.01 then
					self:update_pos()
				end
				self:update_visible(t, dt)
			end
		else
			local hud_panel = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2).panel
			hud_panel:remove(self._spotter_icon)
			managers.hud:remove_updator(self._uptate_id)
		end
		
		self._update_time = not self._update_time
	end
	
	function EnemySpotter:update_pos()		
		self._spotter_icon:set_center(mvector3.x(self._screen_pos), mvector3.y(self._screen_pos))
	end
	
	function EnemySpotter:update_visible(t, dt)	
		local distance = mvector3.distance(managers.player:player_unit():position(), self._unit:position())
		if self._raycast_time and distance < (WOE.ES_MaxDD * 100) then
			self._has_obstruction = false
			local from = managers.player:player_unit():camera():position()
			local raycast = World:raycast("ray", from, self._head_pos, "slot_mask", managers.slot:get_mask("all"))
			if raycast and raycast.unit and not managers.enemy:is_enemy(raycast.unit) then
				if raycast.unit:name():key() ~= "8cc90bccd534e086" then
					self._has_obstruction = true
				end
			end
		end
		
		self._raycast_time = not self._raycast_time
		
		if distance < (WOE.ES_MaxDD * 100) and not self._has_obstruction then
			if not self._spotter_icon:visible() then
				self._spotter_icon:set_visible(true)
			end
			
			if WOE.ES_FadeOut then
					self._spotter_icon:set_alpha(self._spotter_icon:alpha() <= 1 and self._spotter_icon:alpha() + dt*2 or 5)
				else
					if self._spotter_icon:alpha() ~= 5 then
						self._spotter_icon:set_alpha(5)
					end
			end
		else
			if self._screen_pos.z < 0 and self._spotter_icon:alpha() == 5 then
				if self._spotter_icon:visible() then
					self._spotter_icon:set_visible(false)
				end
			else
				if WOE.ES_FadeOut then
					self._spotter_icon:set_alpha(self._spotter_icon:alpha() >= 0 and self._spotter_icon:alpha() - dt*2 or -0.01)
					if self._spotter_icon:alpha() == -0.01 then
						if self._spotter_icon:visible() then
							self._spotter_icon:set_visible(false)
						end
					end
				elseif self._spotter_icon:visible() then
					self._spotter_icon:set_alpha(-0.01)
					self._spotter_icon:set_visible(false)
				end
			end
		end
		
		if self._screen_pos.z > 1 then
			self._spotter_icon:set_visible(true)
		else
			self._spotter_icon:set_visible(false)
		end
	end
	
elseif RequiredScript == "lib/units/enemies/cop/copmovement" then
	local units = {
		taser = {enable = ES_Filter.EST_taser},
		medic = {enable = ES_Filter.EST_medic},
		spooc = {enable = ES_Filter.EST_spooc},
		shield = {enable = ES_Filter.EST_shield},
		tank = {enable = ES_Filter.EST_tank},
		tank_medic = {enable = ES_Filter.EST_tank_medic},
		tank_mini = {enable = ES_Filter.EST_tank_mini},
		sniper = {enable = ES_Filter.EST_sniper},
		heavy_swat_sniper = {enable = ES_Filter.EST_sniper},
		phalanx_minion = {enable = ES_Filter.EST_phalanx_minion},
		snowman_boss = {enable = ES_Filter.EST_snowman_boss},
		summers = {enable = ES_Filter.EST_summers},
		medic_summers = {enable = ES_Filter.EST_medic_summers},
		taser_summers = {enable = ES_Filter.EST_taser_summers},
		tank_titan = {enable = ES_Filter.EST_tank_titan},
		tank_titan_assault = {enable =ES_Filter.EST_tank_titan},
		taser_titan = {enable = ES_Filter.EST_taser_titan},
		hrt_titan = {enable = ES_Filter.EST_hrt_titan},
		phalanx_minion_assault = {enable = ES_Filter.EST_phalanx_minion}
	}

	local CopMovement_init = CopMovement.init
	function CopMovement:init(...)
		CopMovement_init(self, ...)
		
		local current_camera = managers.viewport:get_current_camera()
		local local_player = managers.player:local_player()		
		if self._unit and current_camera and local_player then
			if units[self._unit:base()._tweak_table] and units[self._unit:base()._tweak_table].enable then
				self._unit:base().spotter = EnemySpotter:new()
				self._unit:base().spotter:init(self._unit)
			end
		end
	end
end